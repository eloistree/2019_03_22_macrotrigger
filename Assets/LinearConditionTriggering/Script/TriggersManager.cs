﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggersManager : MonoBehaviour
{
    public OnLinearConditionTriggered m_onConditionTriggered;
    public List<LinearConditionStateMachine> m_triggersPrefab;
    //public List<LinearConditionStateMachine> m_triggersCreated;

    public string m_lastLinearNameTriggered;
    public LinearConditionStateMachine m_lastLinearTriggered;

    public void ProceedWithEntry(EntryEvent entry)
    {
        foreach (var t in m_triggersPrefab)
        {
            t.ProceedWithEntry(entry);
        }
    }

    private void Start()
    {
        for (int i = 0; i < m_triggersPrefab.Count; i++)
        {
            m_triggersPrefab[i].m_onValidated.AddListener( NotifyTheConditionsTriggered);
        }
    }

    private void NotifyTheConditionsTriggered(string name, LinearConditionStateMachine trigger)
    {
        m_onConditionTriggered.Invoke(name, trigger);
        m_lastLinearNameTriggered = name;
        m_lastLinearTriggered = trigger;
        Debug.Log("Oh yeah: " + name, trigger);
    }
}
