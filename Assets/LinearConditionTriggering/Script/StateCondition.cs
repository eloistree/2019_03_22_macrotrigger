﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class StateCondition : MonoBehaviour
{
    public BooleanUnityEvent m_onRunningChange;
    public UnityEvent m_onValidated;
    public UnityEvent m_onIncorrect;

    [SerializeField] bool m_isRunning;

    public bool IsRunnig() {
        return m_isRunning;
    }


    public void StartRunning() {

        ResetAsOrigine();
        DoWhenStartRunning();
        ChangeRunningState(true);
    }

    public void ExitCondition(bool isValide) {

        if (IsRunnig())
        {
            ChangeRunningState(false);

            if (isValide )
                NotifyAsValide();
            if (!isValide)
                NotifyAsIncorrect();

           // ResetAsOrigine();

        }
        
    }


     void NotifyAsValide()
    {
            m_onValidated.Invoke();
       // m_isRunning = false;
        ResetAsOrigine();
    }
   
    void NotifyAsIncorrect()
    {
        m_onIncorrect.Invoke();
       // m_isRunning = false;
        ResetAsOrigine();
    }

     void ChangeRunningState(bool isActive)
    {
        m_isRunning = isActive;
        NotifyRunning(isActive);
    }

     void NotifyRunning(bool isActive)
    {
        m_onRunningChange.Invoke(isActive);
    }


    public abstract void ProceedWithEntry(EntryEvent entry);
    public abstract void ResetAsOrigine();
    public abstract void DoWhenStartRunning();



    [System.Serializable]
    public class BooleanUnityEvent : UnityEvent<bool> { }

}
