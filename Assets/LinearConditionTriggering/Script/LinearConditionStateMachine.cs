﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LinearConditionStateMachine : MonoBehaviour
{
    public string m_idName;
    public List<StateCondition> m_conditions = new List<StateCondition>();
    public OnLinearConditionTriggered m_onValidated;
    public OnLinearConditionTriggered m_onInvalide;


    public bool m_isRunning;
    public float m_timeSinceLaunch;
    public float m_timeSinceLastValide;

    [Header("Debug")]
    public int m_index = 0;
    public bool m_undecideState = true;
    public StateCondition selectedCondition;





    public void ResetAsOrigine() {
        UnlinkAllCondition();
        m_index = 0;
        m_undecideState = true;
        selectedCondition = null;
        m_isRunning = false;
        m_timeSinceLaunch=0;
        m_timeSinceLastValide=0;
        LinkThisCondition(m_index);
    }

    void Start()
    {
        ResetAsOrigine();
    }

    private void Update()
    {
        if (m_isRunning)
        {
            float td = Time.deltaTime;
            m_timeSinceLaunch += td;
            m_timeSinceLastValide += td;
        }
    }

    private void StopMachine()
    {
        if (m_undecideState) {
            m_undecideState = false;
            m_onInvalide.Invoke(m_idName, this);
        }
        
    }
    public void LinkThisCondition(int  conditionIndex)
    {
        if (conditionIndex < 0 || conditionIndex >= m_conditions.Count)
            LinkThisCondition(null);
        else LinkThisCondition(m_conditions[conditionIndex]);

    }
    public void LinkThisCondition(StateCondition condition)
    {
        selectedCondition = condition;
        if (condition != null) {
            condition.m_onValidated.AddListener(NextOrValidate);
            condition.m_onIncorrect.AddListener(StopMachine);
            condition.StartRunning();
        }
        
    }
    public void UnlinkAllCondition() {
        foreach (var c in m_conditions)
        {
            UnlinkThisCondition(c);

        }
    }
    public void UnlinkThisCondition(StateCondition condition)
    {
        if (condition == null)
            return;
        condition.m_onValidated.RemoveListener(NextOrValidate);
        condition.m_onIncorrect.RemoveListener(StopMachine);
    }

    private void NextOrValidate()
    {
        int previousIndex = m_index;
        if (m_conditions.Count == 1) {
            NotifyAsValidated();
        }
        else {

            if (m_index == m_conditions.Count - 1)
                NotifyAsValidated();
            else {
                m_index++;
                UnlinkThisCondition(selectedCondition);
                LinkThisCondition(m_index);
            }
        }

        if (previousIndex != m_index) {
            if (previousIndex == 0) {
                m_isRunning = true;
                m_timeSinceLaunch = 0f;
            }
            m_timeSinceLastValide = 0f;
        }

        

    }
    private void NotifyAsValidated() {
        m_undecideState = false;
        m_onValidated.Invoke(m_idName, this);
    }
    public  void ProceedWithEntry(EntryEvent entry)
    {
        if(m_index<m_conditions.Count)
        m_conditions[m_index].ProceedWithEntry(entry);
    }

}

[System.Serializable]
public class OnLinearConditionTriggered : UnityEvent<string, LinearConditionStateMachine> { }