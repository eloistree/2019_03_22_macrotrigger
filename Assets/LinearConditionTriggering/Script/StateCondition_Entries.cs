﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateCondition_Entries : StateCondition
{
    public string[] m_observedEntries;
    
    
    public ExpectedEntriesType m_entriesType = ExpectedEntriesType.On;
    public enum ExpectedEntriesType { On, Off}
    public bool m_strict;

    public string[] m_entriesToIgnore;

    [Header("Debug")]
    public bool[] m_hasBeenDetected;
    public EntryEvent m_lastEntry;
   // public bool m_alliqeneeeqeuq

    
    public override void ProceedWithEntry(EntryEvent entry)
    {// A BIT MESSY
        m_lastEntry = entry;
        if (!IsRunnig())
            return;

        if (IsToIgnore(entry.m_idName))
            return;

        bool isObserved = IsObservedEntry(entry.m_idName);
        if (m_strict && !isObserved && entry.m_activeState)
        {
            base.ExitCondition(false);
            return;
        }

        if (m_entriesType == ExpectedEntriesType.On)
        {
            for (int i = 0; i < m_observedEntries.Length; i++)
            {
                if (m_observedEntries[i] == entry.m_idName)
                {
                    if(entry.m_activeState)
                        m_hasBeenDetected[i] = true;
                    if (m_hasBeenDetected[i] && entry.m_activeState == false)
                    {
                        base.ExitCondition(false);
                        return;
                    }
                }

            }
            {//Check if is valide
                bool isAllValide = true;
                for (int i = 0; i < m_hasBeenDetected.Length; i++)
                {
                    if (m_hasBeenDetected[i] == false)
                        isAllValide = false;
                }
                if (isAllValide)
                    base.ExitCondition(true);
            }
        }


        if (m_entriesType == ExpectedEntriesType.Off)
        {
            for (int i = 0; i < m_observedEntries.Length; i++)
            {
                if (m_observedEntries[i] == entry.m_idName)
                {
                    if (!entry.m_activeState)
                        m_hasBeenDetected[i] = true;
                    if (m_hasBeenDetected[i] && entry.m_activeState == true)
                    {
                        base.ExitCondition(false);
                        return;
                    }
                }

            }
            {//Check if is valide
                bool isAllValide = true;
                for (int i = 0; i < m_hasBeenDetected.Length; i++)
                {
                    if (m_hasBeenDetected[i] == false)
                        isAllValide = false;
                }
                if (isAllValide)
                    base.ExitCondition(true);
            }
        }



    }

    private bool IsObservedEntry(string m_idName)
    {
        for (int i = 0; i < m_observedEntries.Length; i++)
        {
            if (m_observedEntries[i] == m_idName)
                return true;
        }
        return false;
    }
    private bool IsToIgnore(string m_idName)
    {
        for (int i = 0; i < m_entriesToIgnore.Length; i++)
        {
            if (m_entriesToIgnore[i] == m_idName)
                return true;
        }
        return false;
    }

    public override void DoWhenStartRunning()
    {

        m_hasBeenDetected = new bool[m_observedEntries.Length];
    }

    public override void ResetAsOrigine()
    {

        m_hasBeenDetected = new bool[m_observedEntries.Length];

    }
}
