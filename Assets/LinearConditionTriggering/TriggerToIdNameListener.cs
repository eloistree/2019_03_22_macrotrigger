﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerToIdNameListener : MonoBehaviour
{
    public OnIdNameFound m_found;
    [System.Serializable]
    public class OnIdNameFound : UnityEvent<string> { }

    public void NotifyTheConditionsTriggered(string name, LinearConditionStateMachine trigger)
    {
        m_found.Invoke(name);
    }
}
