﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenshotUtility : Functionable
{
   

    public override string[] GetFonctionsTakeInCharge()
    {
        return new string[] { "TakeScreenshot" };
    }

    void TakeScreenshot(string[] parameters) {
        string path = Application.dataPath + "/../";
        string fileDefault = "Screenshot.png";
        string fullPath = path + fileDefault;
        if (parameters.Length == 1)
            fullPath = parameters[0];
        if (parameters.Length == 2)
            fullPath = parameters[0]+ parameters[1];
        Debug.Log("<<>> " + fullPath);
        ScreenCapture.CaptureScreenshot(fullPath);

    }
}
