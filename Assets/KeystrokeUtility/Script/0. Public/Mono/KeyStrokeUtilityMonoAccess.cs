﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

public class KeyStrokeUtilityMonoAccess : MonoBehaviour {


    public void NotifyNativeChar(char value)
    {

        //   Debug.Log("Hey :-) 1 > " + value);
        KeystrokeUtility.NotifyNativeStroke(value);
    }
    public void NotifyNativeText(string value)
    {

        //   Debug.Log("Hey :-) 2 > " + value);
        KeystrokeUtility.NotifyNativeStroke(value);
    }


    public void NotifyNativeUnityKeyCodeDown(KeyCode value) {
        NotifyNativeUnityKeyCode(value, true); }
    public void NotifyNativeUnityKeyCodeUp(KeyCode value)
    {NotifyNativeUnityKeyCode(value, false);}
    public void NotifyNativeUnityKeyCode(KeyCode value, bool isDown)
    {

        //Debug.Log("Hey :-) 3 > " + value);
        KeystrokeUtility.NotifyNativePress(value, isDown); }

    public void NotifyNativeWindowKeyCodeDown(VirtualKeyCode value)
    {NotifyNativeWindowKeyCode(value, true); }
    public void NotifyNativeWindowKeyCodeUp(VirtualKeyCode value)
    { NotifyNativeWindowKeyCode(value, false); }
    public void NotifyNativeWindowKeyCode(VirtualKeyCode value, bool isDown)
    {
        //Debug.Log("Hey :-) 4 > " + value);
        KeystrokeUtility.NotifyNativePress(value, isDown);
    }


  

    //private void NotifyText(TextStrokeRequest value)
    //{
    //    throw new System.NotImplementedException();
    //}
    //private void NotifyCharacter(CharacterStrokeRequest value)
    //{
    //    throw new System.NotImplementedException();
    //}
    //private void NotifyCharUp(KeyboardCharPressRequest value)
    //{
    //    throw new System.NotImplementedException();

    //}
    //private void NotifyCharDown(KeyboardCharPressRequest value)
    //{
    //    throw new System.NotImplementedException();
    //}
    //private void NotifyUnicode(UnicodeStrokeRequest value)
    //{
    //    throw new System.NotImplementedException();
    //}
    //private void NotifyASCII(AsciiStrokeRequest value)
    //{
    //    throw new System.NotImplementedException();
    //}
    //private void NotifyKeyUp(KeyboardTouchPressRequest value)
    //{
    //    throw new System.NotImplementedException();
    //}
    //private void NotifyKeyDown(KeyboardTouchPressRequest value)
    //{
    //    throw new System.NotImplementedException();
    //}

  
    

  
  

  
}
