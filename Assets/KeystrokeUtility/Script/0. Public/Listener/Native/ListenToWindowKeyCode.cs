﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using WindowsInput;
using WindowsInput.Native;

public class ListenToWindowKeyCode : MonoBehaviour{
    public List<VirtualKeyCode> m_pressed = new List<VirtualKeyCode>();

    [System.Serializable]
    public class OnWindowKeyCodeEvent : UnityEvent<VirtualKeyCode> { }
    public OnWindowKeyCodeEvent m_onKeyCodeDown;
    public OnWindowKeyCodeEvent m_onKeyCodeUp;

    public EnumDictionary<VirtualKeyCode> m_keysState = new EnumDictionary<VirtualKeyCode>();
    public List<VirtualKeyCode> m_unityKeyCodeListened;
    
    protected InputSimulator m_input;
    public bool m_useReal;
    public bool m_debug;
    protected virtual void Awake()
    {
        m_input = new InputSimulator();
        m_keysState.onStateChange += KeyStateChange;
    }

    private void KeyStateChange(VirtualKeyCode element, bool isOn)
    {
        if (isOn)
            m_onKeyCodeDown.Invoke(element);
        else
            m_onKeyCodeUp.Invoke(element);

        if(m_debug)
            Debug.Log("Listen to Virtual Key Code: " + element+"|"+isOn);
    }
    
    protected virtual void Update()
    {
        if(m_input==null)
            m_input = new InputSimulator();
        bool state;
        VirtualKeyCode kc;
        for (int i = 0; i < m_unityKeyCodeListened.Count; i++)
        {
            kc = m_unityKeyCodeListened[i];
            if (!m_useReal)
                state = m_input.InputDeviceState.IsKeyDown(kc);
            else
                state = m_input.InputDeviceState.IsHardwareKeyDown(kc);

            m_keysState.SetState(kc, state);


        }
    }
    void Reset()
    {
        m_unityKeyCodeListened = KeystrokeUtility.GetVirtualKeyCode();
    }
}

