﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ListenToInputField : MonoBehaviour {


    public ListenedInputField[] m_inputFields;

    public void Start()
    {

        ResetListener();
    }


    public void OnValidate()
    {
        ResetListener();
    }

    private void ResetListener()
    {
        for (int i = 0; i < m_inputFields.Length; i++)
        {
            m_inputFields[i].RemoveListenerToSubmit();
            m_inputFields[i].ListenToSubmit();
        }
    }

    [System.Serializable]
    public class OnWordValidated : UnityEvent<string> { };

    [System.Serializable]
    public class ListenedInputField
    {
        public InputField m_inputField;
        public string m_previousState;
        public enum ListeningType { ValueChanged, EndEdition}
        public ListeningType m_listeningType =ListeningType.EndEdition;
      
        public OnWordValidated m_onValidated;

        public void ListenToSubmit()
        {

            if (m_inputField && m_listeningType ==ListeningType.ValueChanged)
                m_inputField.onValueChanged.AddListener(ValueChanged);

            if (m_inputField && m_listeningType == ListeningType.EndEdition)
                m_inputField.onEndEdit.AddListener(EndEdit);
        }

        private void EndEdit(string validate)
        {
            m_inputField.text = "";
            ValueChanged(validate);
        }

        public void RemoveListenerToSubmit()
        {
            if (m_inputField) {

                m_inputField.onValueChanged.RemoveListener(ValueChanged);
            m_inputField.onEndEdit.RemoveListener(EndEdit);
            }
        }

        private void ValueChanged(string value)
        {
            m_previousState = value;
            m_inputField.text = "";
            if (value!=null && value.Length>0)
                m_onValidated.Invoke(value);

        }
    }
        
}
