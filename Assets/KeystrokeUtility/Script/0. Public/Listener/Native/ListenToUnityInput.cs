﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ListenToUnityInput : MonoBehaviour {

    public string m_previousInputCharacters;
    public string m_inputCharacters;
    
    [System.Serializable]
    public class OnKeyCodeEvent : UnityEvent<char> { }
    public OnKeyCodeEvent m_onKeyCodeDown;
    public bool m_debug;

    void Update()
    {
        m_inputCharacters = Input.inputString;
        if (!string.IsNullOrEmpty(m_inputCharacters)) {
            m_previousInputCharacters = m_inputCharacters;

            char kc;
            for (int i = 0; i < m_inputCharacters.Length; i++)
            {
                kc = m_inputCharacters[i];
                if (m_onKeyCodeDown != null)
                    m_onKeyCodeDown.Invoke(kc);
                if (m_debug)
                    Debug.Log("Listent to char: " + kc);

            }
        }
        

    }
    
    
}
