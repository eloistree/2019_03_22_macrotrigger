﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ListenToUnityKeyCode : MonoBehaviour {

    public List<KeyCode> m_pressed = new List<KeyCode>();

    [System.Serializable]
    public class OnKeyCodeEvent : UnityEvent<KeyCode> { }
    public OnKeyCodeEvent m_onKeyCodeDown;
    public OnKeyCodeEvent m_onKeyCodeUp;

    public EnumDictionary<KeyCode> m_keysState = new EnumDictionary<KeyCode>();
    public List<KeyCode> m_unityKeyCodeListened;
   

    public bool m_debug;
    private void Awake()
    {
        m_keysState.onStateChange += KeyStateChange;
    }

    private void KeyStateChange(KeyCode element, bool isOn)
    {
        if (isOn)
            m_onKeyCodeDown.Invoke(element);
        else
            m_onKeyCodeUp.Invoke(element);

        if (m_debug)
            Debug.Log("Listen to Virtual Key Code: " + element+"|"+isOn);
    }

    void Update()
    {
        bool state;
        KeyCode kc;
        for (int i = 0; i < m_unityKeyCodeListened.Count; i++)
        {
            kc = m_unityKeyCodeListened[i];
            state = Input.GetKey(kc);
            m_keysState.SetState(kc, state);

        }
    }
    void Reset()
    {
        m_unityKeyCodeListened = KeystrokeUtility.GetUnityKeyCodes();
    }
}
