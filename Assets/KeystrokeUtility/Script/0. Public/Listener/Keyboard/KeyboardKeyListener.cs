﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public  abstract class KeyboardKeyListener : MonoBehaviour, KeyboardState {


    public KeyboardStorageState m_keyboardTouchState;
    public List<KeyboardTouch> m_activeKeys;
    public List<KeyboardTouch> m_availaibleKeys;

    public OnKeyChange m_onKeyDown;
    public OnKeyChange m_onKeyUp;

    public bool m_isCapsLockOn;
    public bool m_isNumLockOn;
    public bool m_isScrollLock;

    public DateTime lastCheck;
    public DateTime now;

    [System.Serializable]
    public class OnKeyChange : UnityEvent<KeyboardTouch> {
    }
    

    protected virtual void Awake()
    {
        m_keyboardTouchState = new KeyboardStorageState();
        m_availaibleKeys = KeystrokeUtility.GetEnumList<KeyboardTouch>();
        m_keyboardTouchState.m_keyboardState.onStateChange += NotifyEvent;

    } 

    protected virtual void Update()
    {
        now = DateTime.Now;


        m_activeKeys = m_keyboardTouchState.m_keyboardState.GetActiveElements();
        foreach (KeyboardTouch vkc in m_availaibleKeys)
        {
            if (IsTouchActive(vkc))
                m_keyboardTouchState.RealPressDown(vkc);
            if (!IsTouchActive(vkc))
                m_keyboardTouchState.RealPressUp(vkc);
        }
        m_isCapsLockOn = IsCapsLockOn();
        m_isNumLockOn = IsNumLockOn(); 
        m_isScrollLock = IsScrollLockOn();


        lastCheck = now;




    }
    private void NotifyEvent(KeyboardTouch element, bool isOn)
    {
        if (isOn)
            m_onKeyDown.Invoke(element);
        else
            m_onKeyUp.Invoke(element);
    }
  

    void OnValidate()
    {
        m_availaibleKeys = KeystrokeUtility.GetEnumList<KeyboardTouch>();
    }
    private EnumDictionary<KeyboardTouch> GetKeysPresionState()
    {
        return m_keyboardTouchState.m_keyboardState;
    }
    public bool IsAltActive()
    {
        return GetKeysPresionState().GetState(KeyboardTouch.Alt);
    }

   

    public bool IsAltGrActive()
    {
        return GetKeysPresionState().GetState(KeyboardTouch.AltGr);
    }


    public bool IsControlActive()
    {
        return GetKeysPresionState().GetState(KeyboardTouch.Control);
    }

    public bool IsMetaActive()
    {
        return GetKeysPresionState().GetState(KeyboardTouch.Meta);
    }
    public bool IsShiftActive()
    {
        return GetKeysPresionState().GetState(KeyboardTouch.Shift);
    }



    public abstract bool IsCapsLockOn();
    public abstract bool IsNumLockOn();
    public abstract bool IsScrollLockOn();



    public  bool IsTouchDown(KeyboardTouch keyboardTouch) {

        double seconds = (now - lastCheck).TotalSeconds;
        return m_keyboardTouchState.IsTouchDown(keyboardTouch, seconds);
    }
    public abstract bool IsTouchActive(KeyboardTouch keyboardTouch);
    public bool IsTouchUp(KeyboardTouch keyboardTouch)
    {
        double seconds = (now - lastCheck).TotalSeconds;
        return m_keyboardTouchState.IsTouchUp(keyboardTouch, seconds);
    }

    public abstract KeyboardPlatform GetRepresentedPlatform();
    public KeyboardTouch[] GetPressedTouches()
    {
        return m_activeKeys.ToArray();
    }

}
