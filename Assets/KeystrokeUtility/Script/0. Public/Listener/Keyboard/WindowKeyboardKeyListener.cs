﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput;
using WindowsInput.Native;

public class WindowKeyboardKeyListener : KeyboardKeyListener
{
    public InputSimulator m_input;
   public InputSimulator WinInput { get {
            if (m_input == null)
                m_input = new InputSimulator();
            return m_input;
        }
    }

    public override bool IsCapsLockOn()
    {
      return   WinInput.InputDeviceState.IsTogglingKeyInEffect(VirtualKeyCode.CAPITAL);
    }

    public override bool IsNumLockOn()
    {
        return WinInput.InputDeviceState.IsTogglingKeyInEffect(VirtualKeyCode.NUMLOCK);
    }

    public override bool IsScrollLockOn()
    {
        return WinInput.InputDeviceState.IsTogglingKeyInEffect(VirtualKeyCode.SCROLL);
    }

    public override bool IsTouchActive(KeyboardTouch keyboardTouch)
    {
        bool isDown = false;
        List<VirtualKeyCode> corresponding = FindCorrespondance(keyboardTouch);

        foreach (VirtualKeyCode keyCode in corresponding)
        {
            if (WinInput.InputDeviceState.IsKeyDown(keyCode)) {
                isDown = true;
                break;
            }

        }


        return isDown;
    }

    private List<VirtualKeyCode> FindCorrespondance(KeyboardTouch keyboardTouch)
    {
        List<VirtualKeyCode> corresponding = new List<VirtualKeyCode>();
        switch (keyboardTouch)
        {
            case KeyboardTouch.None:
                break;
            case KeyboardTouch.Unkown:
                break;
            case KeyboardTouch.A:
                corresponding.Add(VirtualKeyCode.VK_A);
                break;
            case KeyboardTouch.B:
                corresponding.Add(VirtualKeyCode.VK_B);
                break;
            case KeyboardTouch.C:
                corresponding.Add(VirtualKeyCode.VK_C);
                break;
            case KeyboardTouch.D:
                corresponding.Add(VirtualKeyCode.VK_D);
                break;
            case KeyboardTouch.E:
                corresponding.Add(VirtualKeyCode.VK_E);
                break;
            case KeyboardTouch.F:
                corresponding.Add(VirtualKeyCode.VK_F);
                break;
            case KeyboardTouch.G:
                corresponding.Add(VirtualKeyCode.VK_G);
                break;
            case KeyboardTouch.H:
                corresponding.Add(VirtualKeyCode.VK_H);
                break;
            case KeyboardTouch.I:
                corresponding.Add(VirtualKeyCode.VK_I);
                break;
            case KeyboardTouch.J:
                corresponding.Add(VirtualKeyCode.VK_J);
                break;
            case KeyboardTouch.K:
                corresponding.Add(VirtualKeyCode.VK_K);
                break;
            case KeyboardTouch.L:
                corresponding.Add(VirtualKeyCode.VK_L);
                break;
            case KeyboardTouch.M:
                corresponding.Add(VirtualKeyCode.VK_M);
                break;
            case KeyboardTouch.N:
                corresponding.Add(VirtualKeyCode.VK_N);
                break;
            case KeyboardTouch.O:
                corresponding.Add(VirtualKeyCode.VK_O);
                break;
            case KeyboardTouch.P:
                corresponding.Add(VirtualKeyCode.VK_P);
                break;
            case KeyboardTouch.Q:
                corresponding.Add(VirtualKeyCode.VK_Q);
                break;
            case KeyboardTouch.R:
                corresponding.Add(VirtualKeyCode.VK_R);
                break;
            case KeyboardTouch.S:
                corresponding.Add(VirtualKeyCode.VK_S);
                break;
            case KeyboardTouch.T:
                corresponding.Add(VirtualKeyCode.VK_T);
                break;
            case KeyboardTouch.U:
                corresponding.Add(VirtualKeyCode.VK_U);
                break;
            case KeyboardTouch.V:
                corresponding.Add(VirtualKeyCode.VK_V);
                break;
            case KeyboardTouch.W:
                corresponding.Add(VirtualKeyCode.VK_W);
                break;
            case KeyboardTouch.X:
                corresponding.Add(VirtualKeyCode.VK_X);
                break;
            case KeyboardTouch.Y:
                corresponding.Add(VirtualKeyCode.VK_Y);
                break;
            case KeyboardTouch.Z:
                corresponding.Add(VirtualKeyCode.VK_Z);
                break;
            case KeyboardTouch.NumLock:
                corresponding.Add(VirtualKeyCode.NUMLOCK);
                break;
            case KeyboardTouch.NumPad0:
                corresponding.Add(VirtualKeyCode.NUMPAD0);
                break;
            case KeyboardTouch.NumPad1:
                corresponding.Add(VirtualKeyCode.NUMPAD1);
                break;
            case KeyboardTouch.NumPad2:
                corresponding.Add(VirtualKeyCode.NUMPAD2);
                break;
            case KeyboardTouch.NumPad3:
                corresponding.Add(VirtualKeyCode.NUMPAD3);
                break;
            case KeyboardTouch.NumPad4:
                corresponding.Add(VirtualKeyCode.NUMPAD4);
                break;
            case KeyboardTouch.NumPad5:
                corresponding.Add(VirtualKeyCode.NUMPAD5);
                break;
            case KeyboardTouch.NumPad6:
                corresponding.Add(VirtualKeyCode.NUMPAD6);
                break;
            case KeyboardTouch.NumPad7:
                corresponding.Add(VirtualKeyCode.NUMPAD7);
                break;
            case KeyboardTouch.NumPad8:
                corresponding.Add(VirtualKeyCode.NUMPAD8);
                break;
            case KeyboardTouch.NumPad9:
                corresponding.Add(VirtualKeyCode.NUMPAD9);
                break;
            case KeyboardTouch.NumPadDecimal:
                corresponding.Add(VirtualKeyCode.DECIMAL);
                break;
            case KeyboardTouch.NumPadDivide:
                corresponding.Add(VirtualKeyCode.DIVIDE);
                break;
            case KeyboardTouch.NumPadMultiply:
                corresponding.Add(VirtualKeyCode.MULTIPLY);
                break;
            case KeyboardTouch.NumPadSubstract:
                corresponding.Add(VirtualKeyCode.SUBTRACT);
                break;
            case KeyboardTouch.NumPadAdd:
                corresponding.Add(VirtualKeyCode.ADD);
                break;
            case KeyboardTouch.NumPadEnter:
                break;
            case KeyboardTouch.NumPadPeriod:
                break;
            case KeyboardTouch.NumPadEquals:
                break;
            case KeyboardTouch.NumPadClear:
                corresponding.Add(VirtualKeyCode.CLEAR);
                break;
            case KeyboardTouch.AllClear:
                break;
            case KeyboardTouch.AC:
                break;
            case KeyboardTouch.NumPadClearEntry:
                break;
            case KeyboardTouch.ClearEntry:
                break;
            case KeyboardTouch.CE:
                break;
            case KeyboardTouch.NumpadMemoryAdd:
                break;
            case KeyboardTouch.MA:
                break;
            case KeyboardTouch.NumpadMemoryClear:
                break;
            case KeyboardTouch.MC:
                break;
            case KeyboardTouch.NumpadMemoryRecall:
                break;
            case KeyboardTouch.MR:
                break;
            case KeyboardTouch.NumpadMemoryStore:
                break;
            case KeyboardTouch.MS:
                break;
            case KeyboardTouch.NumpadMemorySubtract:
                break;
            case KeyboardTouch.MSub:
                break;
            case KeyboardTouch.Alpha0:
                corresponding.Add(VirtualKeyCode.VK_0);
                break;
            case KeyboardTouch.Alpha1:
                corresponding.Add(VirtualKeyCode.VK_1);
                break;
            case KeyboardTouch.Alpha2:
                corresponding.Add(VirtualKeyCode.VK_2);
                break;
            case KeyboardTouch.Alpha3:
                corresponding.Add(VirtualKeyCode.VK_3);
                break;
            case KeyboardTouch.Alpha4:
                corresponding.Add(VirtualKeyCode.VK_4);
                break;
            case KeyboardTouch.Alpha5:
                corresponding.Add(VirtualKeyCode.VK_5);
                break;
            case KeyboardTouch.Alpha6:
                corresponding.Add(VirtualKeyCode.VK_6);
                break;
            case KeyboardTouch.Alpha7:
                corresponding.Add(VirtualKeyCode.VK_7);
                break;
            case KeyboardTouch.Alpha8:
                corresponding.Add(VirtualKeyCode.VK_8);
                break;
            case KeyboardTouch.Alpha9:
                corresponding.Add(VirtualKeyCode.VK_9);
                break;
            case KeyboardTouch.Up:
                corresponding.Add(VirtualKeyCode.UP);
                break;
            case KeyboardTouch.Down:
                corresponding.Add(VirtualKeyCode.DOWN);
                break;
            case KeyboardTouch.Right:
                corresponding.Add(VirtualKeyCode.RIGHT);
                break;
            case KeyboardTouch.Left:
                corresponding.Add(VirtualKeyCode.LEFT);
                break;
            case KeyboardTouch.PageUp:
                corresponding.Add(VirtualKeyCode.PRIOR);
                break;
            case KeyboardTouch.PageDown:
                corresponding.Add(VirtualKeyCode.NEXT);
                break;
            case KeyboardTouch.Home:
                corresponding.Add(VirtualKeyCode.HOME);
                break;
            case KeyboardTouch.End:
                corresponding.Add(VirtualKeyCode.END);
                break;
            case KeyboardTouch.Insert:
                corresponding.Add(VirtualKeyCode.INSERT);
                break;
            case KeyboardTouch.Delete:
                corresponding.Add(VirtualKeyCode.DELETE);
                break;
            case KeyboardTouch.DEL:
                corresponding.Add(VirtualKeyCode.DELETE);
                break;
            case KeyboardTouch.F1:
                corresponding.Add(VirtualKeyCode.F1);
                break;
            case KeyboardTouch.F2:
                corresponding.Add(VirtualKeyCode.F2);
                break;
            case KeyboardTouch.F3:
                corresponding.Add(VirtualKeyCode.F3);
                break;
            case KeyboardTouch.F4:
                corresponding.Add(VirtualKeyCode.F4);
                break;
            case KeyboardTouch.F5:
                corresponding.Add(VirtualKeyCode.F5);
                break;
            case KeyboardTouch.F6:
                corresponding.Add(VirtualKeyCode.F6);
                break;
            case KeyboardTouch.F7:
                corresponding.Add(VirtualKeyCode.F7);
                break;
            case KeyboardTouch.F8:
                corresponding.Add(VirtualKeyCode.F8);
                break;
            case KeyboardTouch.F9:
                corresponding.Add(VirtualKeyCode.F9);
                break;
            case KeyboardTouch.F10:
                corresponding.Add(VirtualKeyCode.F10);
                break;
            case KeyboardTouch.F11:
                corresponding.Add(VirtualKeyCode.F11);
                break;
            case KeyboardTouch.F12:
                corresponding.Add(VirtualKeyCode.F12);
                break;
            case KeyboardTouch.F13:
                corresponding.Add(VirtualKeyCode.F13);
                break;
            case KeyboardTouch.F14:
                corresponding.Add(VirtualKeyCode.F14);
                break;
            case KeyboardTouch.F15:
                corresponding.Add(VirtualKeyCode.F15);
                break;
            case KeyboardTouch.F16:
                corresponding.Add(VirtualKeyCode.F16);
                break;
            case KeyboardTouch.F17:
                corresponding.Add(VirtualKeyCode.F17);
                break;
            case KeyboardTouch.F18:
                corresponding.Add(VirtualKeyCode.F18);
                break;
            case KeyboardTouch.F19:
                corresponding.Add(VirtualKeyCode.F19);
                break;
            case KeyboardTouch.F20:
                corresponding.Add(VirtualKeyCode.F20);
                break;
            case KeyboardTouch.F21:
                corresponding.Add(VirtualKeyCode.F21);
                break;
            case KeyboardTouch.F22:
                corresponding.Add(VirtualKeyCode.F22);
                break;
            case KeyboardTouch.F23:
                corresponding.Add(VirtualKeyCode.F23);
                break;
            case KeyboardTouch.F24:
                corresponding.Add(VirtualKeyCode.F24);
                break;
            case KeyboardTouch.WinUS_OEM_Comma:
                corresponding.Add(VirtualKeyCode.OEM_COMMA);
                break;
            case KeyboardTouch.WinUS_OEM_Period:
                corresponding.Add(VirtualKeyCode.OEM_PERIOD);
                break;
            case KeyboardTouch.WinUS_OEM_Minus:
                corresponding.Add(VirtualKeyCode.OEM_MINUS);
                break;
            case KeyboardTouch.WinUS_OEM_PLUS:
                corresponding.Add(VirtualKeyCode.OEM_PLUS);
                break;
            case KeyboardTouch.WinUS_OEM_102_BackSlash:
                corresponding.Add(VirtualKeyCode.OEM_102);
                break;
            case KeyboardTouch.WinUS_OEM_1_SemiColon:
                corresponding.Add(VirtualKeyCode.OEM_1);
                break;
            case KeyboardTouch.WinUS_OEM_2_Slash:
                corresponding.Add(VirtualKeyCode.OEM_2);
                break;
            case KeyboardTouch.WinUS_OEM_3_GraveAccent:
                corresponding.Add(VirtualKeyCode.OEM_3);
                break;
            case KeyboardTouch.WinUS_OEM_4_LeftBracket:
                corresponding.Add(VirtualKeyCode.OEM_4);
                break;
            case KeyboardTouch.WinUS_OEM_5_BackSlash:
                corresponding.Add(VirtualKeyCode.OEM_5);
                break;
            case KeyboardTouch.WinUS_OEM_6_RightBlacket:
                corresponding.Add(VirtualKeyCode.OEM_6);
                break;
            case KeyboardTouch.WinUS_OEM_7_Quote:
                corresponding.Add(VirtualKeyCode.OEM_7);
                break;
            case KeyboardTouch.Space:
                corresponding.Add(VirtualKeyCode.SPACE);
                break;
            case KeyboardTouch.SP:
                corresponding.Add(VirtualKeyCode.SPACE);
                break;
            case KeyboardTouch.CapsLock:
                corresponding.Add(VirtualKeyCode.CAPITAL);
                break;
            case KeyboardTouch.Backspace:
                corresponding.Add(VirtualKeyCode.BACK);
                break;
            case KeyboardTouch.BS:
                corresponding.Add(VirtualKeyCode.BACK);
                break;
            case KeyboardTouch.Tab:
                corresponding.Add(VirtualKeyCode.TAB);
                break;
            case KeyboardTouch.HT:
                corresponding.Add(VirtualKeyCode.TAB);
                break;
            case KeyboardTouch.Shift:
                corresponding.Add(VirtualKeyCode.SHIFT);
                corresponding.Add(VirtualKeyCode.LSHIFT);
                corresponding.Add(VirtualKeyCode.RSHIFT);
                break;
            case KeyboardTouch.LeftShift:
                corresponding.Add(VirtualKeyCode.LSHIFT);
                break;
            case KeyboardTouch.RightShift:
                corresponding.Add(VirtualKeyCode.RSHIFT);
                break;
            case KeyboardTouch.Control:
                corresponding.Add(VirtualKeyCode.CONTROL);
                corresponding.Add(VirtualKeyCode.LCONTROL);
                corresponding.Add(VirtualKeyCode.RCONTROL);
                break;
            case KeyboardTouch.LeftControl:
                corresponding.Add(VirtualKeyCode.LCONTROL);
                break;
            case KeyboardTouch.RightControl:
                corresponding.Add(VirtualKeyCode.RCONTROL);
                break;
            case KeyboardTouch.Menu:
                corresponding.Add(VirtualKeyCode.MENU);
                break;
            case KeyboardTouch.Alt:
                corresponding.Add(VirtualKeyCode.MENU);
                break;
            case KeyboardTouch.AltGr:
                corresponding.Add(VirtualKeyCode.RMENU);
                break;
            case KeyboardTouch.LeftAlt:
                corresponding.Add(VirtualKeyCode.LMENU);
                break;
            case KeyboardTouch.RightAlt:
                corresponding.Add(VirtualKeyCode.RMENU);
                break;
            case KeyboardTouch.ContextMenu:
                corresponding.Add(VirtualKeyCode.APPS);
                break;
            case KeyboardTouch.Application:
                corresponding.Add(VirtualKeyCode.APPS);
                break;
            case KeyboardTouch.Break:
                corresponding.Add(VirtualKeyCode.PAUSE);
                break;
            case KeyboardTouch.ScrollLock:
                corresponding.Add(VirtualKeyCode.SCROLL);
                break;
            case KeyboardTouch.Print:
                corresponding.Add(VirtualKeyCode.PRINT);
                break;
            case KeyboardTouch.PrintScreen:
                corresponding.Add(VirtualKeyCode.SNAPSHOT);
                break;
            case KeyboardTouch.Help:
                corresponding.Add(VirtualKeyCode.HELP);
                break;
            case KeyboardTouch.SystemRequest:
                break;
            case KeyboardTouch.MouseLeft:
                corresponding.Add(VirtualKeyCode.LBUTTON);
                break;
            case KeyboardTouch.MouseRight:
                corresponding.Add(VirtualKeyCode.RBUTTON);
                break;
            case KeyboardTouch.MouseMiddle:
                corresponding.Add(VirtualKeyCode.MBUTTON);
                break;
            case KeyboardTouch.MouseSupp1:
                corresponding.Add(VirtualKeyCode.XBUTTON1);
                break;
            case KeyboardTouch.MouseSupp2:
                corresponding.Add(VirtualKeyCode.XBUTTON2);
                break;
            case KeyboardTouch.MouseSupp3:
                break;
            case KeyboardTouch.EndOfText:
                break;
            case KeyboardTouch.ETX:
                break;
            case KeyboardTouch.Escape:
                corresponding.Add(VirtualKeyCode.ESCAPE);
                break;
            case KeyboardTouch.ESC:
                corresponding.Add(VirtualKeyCode.ESCAPE);
                break;
            case KeyboardTouch.Null:
                break;
            case KeyboardTouch.NUL:
                break;
            case KeyboardTouch.InformationSeparatorOne:
                break;
            case KeyboardTouch.US:
                break;
            case KeyboardTouch.InformationSeparatorTwo:
                break;
            case KeyboardTouch.RS:
                break;
            case KeyboardTouch.InformationSeparatorThree:
                break;
            case KeyboardTouch.GS:
                break;
            case KeyboardTouch.InformationSeparatorFour:
                break;
            case KeyboardTouch.FS:
                break;
            case KeyboardTouch.LineFeed:
                break;
            case KeyboardTouch.LF:
                break;
            case KeyboardTouch.Return:
                corresponding.Add(VirtualKeyCode.RETURN);
                break;
            case KeyboardTouch.Enter:
                corresponding.Add(VirtualKeyCode.RETURN);
                break;
            case KeyboardTouch.CarriageReturn:
                corresponding.Add(VirtualKeyCode.RETURN);
                break;
            case KeyboardTouch.CR:
                corresponding.Add(VirtualKeyCode.RETURN);
                break;
            case KeyboardTouch.Eject:
                break;
            case KeyboardTouch.LaunchApplicationOne:
                corresponding.Add(VirtualKeyCode.LAUNCH_APP1);
                break;
            case KeyboardTouch.LaunchApplicationTwo:
                corresponding.Add(VirtualKeyCode.LAUNCH_APP2);
                break;
            case KeyboardTouch.LaunchMail:
                corresponding.Add(VirtualKeyCode.LAUNCH_MAIL);
                break;
            case KeyboardTouch.Sleep:
                corresponding.Add(VirtualKeyCode.SLEEP);
                break;
            case KeyboardTouch.Power:
                break;
            case KeyboardTouch.WakeUp:
                break;
            case KeyboardTouch.Select:
                corresponding.Add(VirtualKeyCode.SELECT);
                break;
            case KeyboardTouch.Execute:
                corresponding.Add(VirtualKeyCode.EXECUTE);
                break;
            case KeyboardTouch.Clear:
                corresponding.Add(VirtualKeyCode.CLEAR);
                break;
            case KeyboardTouch.NextTrack:
                corresponding.Add(VirtualKeyCode.MEDIA_NEXT_TRACK);
                break;
            case KeyboardTouch.PreviousTrack:
                corresponding.Add(VirtualKeyCode.MEDIA_PREV_TRACK);
                break;
            case KeyboardTouch.StopTrack:
                corresponding.Add(VirtualKeyCode.MEDIA_STOP);
                break;
            case KeyboardTouch.Play:
                corresponding.Add(VirtualKeyCode.MEDIA_PLAY_PAUSE);
                break;
            case KeyboardTouch.Pause:
                corresponding.Add(VirtualKeyCode.MEDIA_PLAY_PAUSE);
                break;
            case KeyboardTouch.VolumeMute:
                corresponding.Add(VirtualKeyCode.VOLUME_MUTE);
                break;
            case KeyboardTouch.VolumeDown:
                corresponding.Add(VirtualKeyCode.VOLUME_DOWN);
                break;
            case KeyboardTouch.VolumeUp:
                corresponding.Add(VirtualKeyCode.VOLUME_UP);
                break;
            case KeyboardTouch.Zoom:
                corresponding.Add(VirtualKeyCode.ZOOM);
                break;
            case KeyboardTouch.BrowserBack:
                corresponding.Add(VirtualKeyCode.BROWSER_BACK);
                break;
            case KeyboardTouch.BrowserForward:
                corresponding.Add(VirtualKeyCode.BROWSER_FORWARD);
                break;
            case KeyboardTouch.BrowserFavorites:
                corresponding.Add(VirtualKeyCode.BROWSER_FAVORITES);
                break;
            case KeyboardTouch.BrowserHome:
                corresponding.Add(VirtualKeyCode.BROWSER_HOME);
                break;
            case KeyboardTouch.BrowserRefresh:
                corresponding.Add(VirtualKeyCode.BROWSER_REFRESH);
                break;
            case KeyboardTouch.BrowserSearch:
                corresponding.Add(VirtualKeyCode.BROWSER_SEARCH);
                break;
            case KeyboardTouch.BrowserStop:
                corresponding.Add(VirtualKeyCode.BROWSER_STOP);
                break;
            case KeyboardTouch.Meta:
                corresponding.Add(VirtualKeyCode.LWIN);
                corresponding.Add(VirtualKeyCode.RWIN);
                break;
            case KeyboardTouch.MetaLeft:
                corresponding.Add(VirtualKeyCode.LWIN);
                break;
            case KeyboardTouch.MetaRight:
                corresponding.Add(VirtualKeyCode.RWIN);
                break;
            case KeyboardTouch.LeftWindow:
                corresponding.Add(VirtualKeyCode.LWIN);
                break;
            case KeyboardTouch.RightWindow:
                corresponding.Add(VirtualKeyCode.RWIN);
                break;
            case KeyboardTouch.LeftCommand:
                break;
            case KeyboardTouch.RightCommand:
                break;
            case KeyboardTouch.Convert:
                corresponding.Add(VirtualKeyCode.CONVERT);
                break;
            case KeyboardTouch.KanaMode:
                corresponding.Add(VirtualKeyCode.KANA);
                break;
            case KeyboardTouch.Lang1:
                break;
            case KeyboardTouch.Lang2:
                break;
            case KeyboardTouch.Lang3:
                break;
            case KeyboardTouch.Lang4:
                break;
            case KeyboardTouch.Lang5:
                break;
            case KeyboardTouch.NonConvert:
                corresponding.Add(VirtualKeyCode.NONCONVERT);
                break;
            default:
                break;
        }


        return corresponding;
    }

   

    public override KeyboardPlatform GetRepresentedPlatform()
    {
        return KeyboardPlatform.Window;
    }
    
}
