﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput;
using WindowsInput.Native;

public class WindowVirtualKeyboard : AbstractInteractableKeyboard
{

    public bool m_isOnWritePlatform;
    public bool m_checkHardWareKeyState;
    public InputSimulator m_updatedInstance;
    public InputSimulator WinKeyboard { get {  
                if(m_updatedInstance==null)
                m_updatedInstance =new InputSimulator();
            return m_updatedInstance;
            // m_updatedInstance; 
        } }

    public bool m_useDebug;

    public void Awake()
    {
        CheckIfRunable();
    }
    public void OnValidate()
    {
        CheckIfRunable();
    }

    private void CheckIfRunable()
    {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        m_isOnWritePlatform = true;
#endif
    }
    

    public override bool GetRealStateOf(KeyboardTouch touch)
    {
        bool isConvertable;
        VirtualKeyCode virtualKeyCode;
        KeyBindingTable.ConvertTouchToWindowVirtualKeyCodes(touch, out virtualKeyCode, out isConvertable);
        if (!isConvertable) {
            if(m_useDebug)
             Debug.Log("Real State | Not convertable:" + virtualKeyCode.ToString() + " of "+ touch);
            return false;
        }
        if(m_checkHardWareKeyState)
            return WinKeyboard.InputDeviceState.IsHardwareKeyDown(virtualKeyCode);
        return WinKeyboard.InputDeviceState.IsKeyDown(virtualKeyCode);
    }


    public override bool GetRealStateOfCapsLock()
    {
        return WinKeyboard.InputDeviceState.IsTogglingKeyInEffect(VirtualKeyCode.CAPITAL);
    }

    public override bool GetRealStateOfNumLock()
    {
        return WinKeyboard.InputDeviceState.IsTogglingKeyInEffect(VirtualKeyCode.NUMLOCK);
    }

    public override bool GetRealStateOfScrollLock()
    {
        return WinKeyboard.InputDeviceState.IsTogglingKeyInEffect(VirtualKeyCode.SCROLL);
    }

    public override  void RealPressDown(KeyboardTouch touch)
    {
        bool isConvertable;
        VirtualKeyCode virtualKeyCode;
        KeyBindingTable.ConvertTouchToWindowVirtualKeyCodes(touch, out virtualKeyCode, out isConvertable);
        if (!isConvertable)
        {
            if(m_useDebug)
            Debug.Log("Press Down | Not convertable:" + touch.ToString());
            return;
        }
        if (MousePressing(touch, true)) { }
        else    WinKeyboard.Keyboard.KeyDown(virtualKeyCode);
    }

    public override void RealPressUp(KeyboardTouch touch)
    {
        bool isConvertable;
        VirtualKeyCode virtualKeyCode;
        KeyBindingTable.ConvertTouchToWindowVirtualKeyCodes(touch, out virtualKeyCode, out isConvertable);
        if (!isConvertable)
        {
            Debug.Log("Press Up | Not convertable:" + touch.ToString());
            return;
        }
        if(MousePressing(touch, false)){}
        else { WinKeyboard.Keyboard.KeyUp(virtualKeyCode); }
    }

    private bool  MousePressing( KeyboardTouch touch, bool down)
    {
        if (touch == KeyboardTouch.MouseLeft || touch == KeyboardTouch.MouseMiddle || touch == KeyboardTouch.MouseRight
             || touch == KeyboardTouch.MouseSupp1 || touch == KeyboardTouch.MouseSupp2) {

                switch (touch)
                {
                case KeyboardTouch.MouseLeft:
                    if (down)
                        WinKeyboard.Mouse.LeftButtonDown();
                    else
                        WinKeyboard.Mouse.LeftButtonUp();
                    break;
                case KeyboardTouch.MouseRight:
                    if (down)
                        WinKeyboard.Mouse.RightButtonDown();
                    else
                        WinKeyboard.Mouse.RightButtonUp();
                    break;
                case KeyboardTouch.MouseMiddle:
                case KeyboardTouch.MouseSupp1:
                case KeyboardTouch.MouseSupp2:
                case KeyboardTouch.MouseSupp3:
                    throw new NotImplementedException("InputSimulaotr Don't seem to work with Mouse.XButton and Keyboard.Press (XBUTTON1)");

                default:
                    break;
                }
            return true;
        }
        return false;
    }

    public override void Stroke(char character)
    {
        WinKeyboard.Keyboard.TextEntry(character);
    }
    public override  void Stroke(string text)
    {
        WinKeyboard.Keyboard.TextEntry(text);
    }
    public void StrokeUnicode(int unicode) {
        Stroke((char)unicode);
    }
    
}


public abstract class AbstractInteractableKeyboard : AbstractKeyboardState, InteractableKeyboard {

    public void CheckThatKeypadIsOn()
    {
        bool isNumOn = GetRealStateOfNumLock();
        if (!isNumOn)
        {
            PressDown(KeyboardTouch.NumLock);
            PressUp(KeyboardTouch.NumLock);
        }
    }


    public void PressDown(KeyboardTouch touch)
    {
        Debug.Log("Hey 1 :) > " + touch);

        m_keyboardState.RealPressDown(touch);
        RealPressDown(touch);
    }
    public void PressUp(KeyboardTouch touch)
    {
        m_keyboardState.RealPressUp(touch);
        RealPressUp(touch);
    }

    public abstract void RealPressDown(KeyboardTouch touch);
    public abstract void RealPressUp(KeyboardTouch touch);

    public abstract void Stroke(char character);
    public abstract void Stroke(string text);

    
}

//DIVIVE THE CLASS IN TO Mono<-AbstractKeyboardStateAccess<-AsbtractKeyboardAccess
public abstract class AbstractKeyboardState : MonoBehaviour, KeyboardState {

    public KeyboardStorageState m_keyboardState;
    public DateTime m_lastCheck;
    public DateTime m_now;
    public double m_timeDelta;

    // Use this for initialization
    void Awake()
    {
        m_keyboardState.Reset();
        RefreshToRealState();
        

    }
    void Update() {

        m_now = DateTime.Now;
        RefreshToRealState();
        m_timeDelta = (m_now - m_lastCheck).TotalSeconds; 
        m_lastCheck = m_now;

    }
    

    private void RefreshToRealState()
    {
        KeyboardTouch[] touches = KeyBindingTable.GetAllTouches();
        for (int i = 0; i < touches.Length; i++)
        {
            if(GetRealStateOf(touches[i]))
                m_keyboardState.RealPressDown(touches[i]);
            else
                m_keyboardState.RealPressUp(touches[i]);
        }
    }

    public abstract bool GetRealStateOf(KeyboardTouch touch);
    public abstract bool GetRealStateOfCapsLock();
    public abstract bool GetRealStateOfNumLock();
    public abstract bool GetRealStateOfScrollLock();

    public bool IsTouchActive(KeyboardTouch keyboardTouch)
    {
      return   m_keyboardState.IsTouchActive(keyboardTouch);
    }

    public bool IsTouchUp(KeyboardTouch keyboardTouch)
    {
        return m_keyboardState.IsTouchUp(keyboardTouch, m_timeDelta);
    }
    public bool IsTouchDown(KeyboardTouch keyboardTouch)
    {
        return m_keyboardState.IsTouchDown(keyboardTouch,m_timeDelta);
    }
    public bool IsTouchUp(KeyboardTouch keyboardTouch, double time )
    {
        return m_keyboardState.IsTouchUp(keyboardTouch, m_timeDelta);
    }
    public bool IsTouchDown(KeyboardTouch keyboardTouch, double time)
    {
        return m_keyboardState.IsTouchDown(keyboardTouch, m_timeDelta);
    }
    public bool IsControlActive()
    {
        return m_keyboardState.IsControlActive();
    }

    public bool IsShiftActive()
    {
        return m_keyboardState.IsShiftActive();
    }

    public bool IsMetaActive()
    {
        return m_keyboardState.IsMetaActive();
    }

    public bool IsAltActive()
    {
        return m_keyboardState.IsAltActive();
    }

    public bool IsAltGrActive()
    {
        return m_keyboardState.IsAltGrActive();
    }

    public bool IsCapsLockOn()
    {
        return m_keyboardState.IsCapsLockOn();
    }

    public bool IsNumLockOn()
    {
        return m_keyboardState.IsNumLockOn();
    }

    public bool IsScrollLockOn()
    {
        return m_keyboardState.IsScrollLockOn();
    }

    public KeyboardPlatform GetRepresentedPlatform()
    {
        return m_keyboardState.GetRepresentedPlatform();
    }

    public KeyboardTouch[] GetPressedTouches()
    {
        return m_keyboardState.GetPressedTouches();
    }
}


[System.Serializable]
public class KeyboardStorageState :KeyboardState, InteractableKeyboard
{
    public KeyboardPlatform m_representedPlatform = KeyboardPlatform.Unknow;
    public EnumDictionary<KeyboardTouch> m_keyboardState = new EnumDictionary<KeyboardTouch>();
    public TimeDictionary<KeyboardTouch> m_downTime = new TimeDictionary<KeyboardTouch>();
    public TimeDictionary<KeyboardTouch> m_upTime = new TimeDictionary<KeyboardTouch>();
    public bool m_capsLockState;
    public bool m_numLockState;
    public bool m_scrollLockState;



    public bool IsAltActive()
    {

        return IsTouchActive(KeyboardTouch.Alt) || IsTouchActive(KeyboardTouch.AltGr) || IsTouchActive(KeyboardTouch.LeftAlt) || IsTouchActive(KeyboardTouch.RightAlt);
    }

    public bool IsAltGrActive()
    {
        return IsTouchActive(KeyboardTouch.AltGr) || IsTouchActive(KeyboardTouch.RightAlt);
    }

    public bool IsCapsLockOn()
    {
        return m_capsLockState;
    }

    public bool IsControlActive()
    {
        return IsTouchActive(KeyboardTouch.Control) || IsTouchActive(KeyboardTouch.LeftControl) || IsTouchActive(KeyboardTouch.RightControl);
    }

    public bool IsMetaActive()
    {
        return IsTouchActive(KeyboardTouch.Meta) || IsTouchActive(KeyboardTouch.MetaLeft) || IsTouchActive(KeyboardTouch.MetaRight)
            || IsTouchActive(KeyboardTouch.LeftWindow) || IsTouchActive(KeyboardTouch.RightWindow)
             || IsTouchActive(KeyboardTouch.RightCommand) || IsTouchActive(KeyboardTouch.LeftCommand)
             ;
    }

    public bool IsNumLockOn()
    {
        return m_numLockState;
    }

    public bool IsScrollLockOn()
    {
        return m_scrollLockState;
    }

    public bool IsShiftActive()
    {
        return IsTouchActive(KeyboardTouch.Shift) || IsTouchActive(KeyboardTouch.LeftShift) || IsTouchActive(KeyboardTouch.RightShift);
    }
    public bool IsTouchActive(KeyboardTouch keyboardTouch)
    {
        return m_keyboardState.GetState(keyboardTouch) == true;
    }


    public bool IsTouchDown(KeyboardTouch keyboardTouch) {
       return  IsTouchDown(keyboardTouch, 0);
    }
    public bool IsTouchDown(KeyboardTouch keyboardTouch, double frameTime)
    {
        //bool hasBeenUsed = m_downTime.HasBeenUsedYet(keyboardTouch);
        //if (!hasBeenUsed) return false;
        DateTime time = m_downTime.GetTime(keyboardTouch);
        double timePast = m_downTime.GetTimeInSecondsBetween(time, DateTime.Now);
        if (frameTime == 0f)
            frameTime = Time.deltaTime;
        bool result = timePast < frameTime;
        //if (result)
        //    Debug.Log("TIME "+ (result ? "1" : "0")+": " + timePast  +" < "+frameTime+"  "+ keyboardTouch );


        return result;
    }
    public bool IsTouchUp(KeyboardTouch keyboardTouch)
    {

        return IsTouchUp(keyboardTouch);
    }
    public bool IsTouchUp(KeyboardTouch keyboardTouch, double frameTime)
    {
        //bool hasBeenUsed = m_upTime.HasBeenUsedYet(keyboardTouch);
        //if (!hasBeenUsed) return true;
        DateTime time = m_upTime.GetTime(keyboardTouch);
        double timePast = m_upTime.GetTimeInSecondsBetween(time, DateTime.Now);

        bool result = timePast < frameTime;
        //if(result)
        //    Debug.Log("TIME "+ (result ? "1" : "0")+": " + timePast  +" < "+frameTime+"  "+ keyboardTouch );
        return result;
    }

    public KeyboardPlatform GetRepresentedPlatform()
    {
        return m_representedPlatform;
    }

    public KeyboardTouch[] GetPressedTouches()
    {
        return m_keyboardState.GetActiveElements().ToArray();
    }

    internal void Reset()
    {
        m_keyboardState.Clear();
    }

    public void RealPressDown(KeyboardTouch touch)
    {
        bool hasChanged=m_keyboardState.GetState(touch) == false;
        if (hasChanged) {
            if (touch == KeyboardTouch.CapsLock)
                m_capsLockState = !m_capsLockState;
            if (touch == KeyboardTouch.NumLock)
                m_numLockState = !m_numLockState;
            if (touch == KeyboardTouch.ScrollLock)
                m_scrollLockState = !m_scrollLockState;
            m_downTime.SetTimeRecorded(touch, DateTime.Now);
        }
        m_keyboardState.SetState(touch, true);
    }


    public void RealPressUp(KeyboardTouch touch)
    {
        bool hasChanged = m_keyboardState.GetState(touch) == true;
        if (hasChanged)
        {

            m_keyboardState.SetState(touch, false);
            m_upTime.SetTimeRecorded(touch, DateTime.Now);
        }
    }

    public void Stroke(string text)
    {

    }

    public void Stroke(char character)
    {
    }
}
