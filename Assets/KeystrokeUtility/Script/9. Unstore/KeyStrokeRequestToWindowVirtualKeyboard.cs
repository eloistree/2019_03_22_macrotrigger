﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyStrokeRequestToWindowVirtualKeyboard :MonoBehaviour, Keyboardable {

    public AbstractInteractableKeyboard m_windowKeyboard;

    public void PressKey(KeyboardTouchPressRequest key)
    {
        Debug.Log("Hey :) 2 > " + key);
        if(key.m_pression == PressType.Down || key.m_pression == PressType.Both)
            m_windowKeyboard.PressDown(key.m_touch);

        if (key.m_pression == PressType.Up || key.m_pression == PressType.Both)
            m_windowKeyboard.PressUp(key.m_touch);

    }



    public void PressKey(KeyboardCharPressRequest character)
    {
        // Use convertable table to check  what Touch correspond to a letter depending of the keyboard
        
        //if (key.m_pression == PressType.Down || key.m_pression == PressType.Both)
        //    m_windowKeyboard.PressDown(key.m_touch);

        //if (key.m_pression == PressType.Up || key.m_pression == PressType.Both)
        //    m_windowKeyboard.PressUp(key.m_touch);
        throw new System.NotImplementedException();
    }

    public void Stroke(CharacterStrokeRequest character)
    {
        m_windowKeyboard.Stroke(character.m_character);
    }
    public void Stroke(TextStrokeRequest text)
    {
        m_windowKeyboard.Stroke(text.m_text);
    }

    public void StrokeASCII(AsciiStrokeRequest asciiCode)
    {
       // m_windowKeyboard.CheckThatKeypadIsOn();
       // m_windowKeyboard.PressDown(KeyboardTouch.Alt);
        KeyboardTouch[] touches = KeyBindingTable.GetKeyPadSequenceOf(asciiCode.m_code);
        for (int i = 0; i < touches.Length; i++)
        {
            m_windowKeyboard.PressDown(touches[i]);
            m_windowKeyboard.PressUp(touches[i]);
        }
        // m_windowKeyboard.PressUp(KeyboardTouch.Alt);
    }

    public void StrokeUnicode(UnicodeStrokeRequest unicode)
    {
        bool isConvertable;
        char character;
        KeyBindingTable.ConvertUnicodeToChar(unicode.m_code, out character, out isConvertable);
        if(isConvertable)
            m_windowKeyboard.Stroke(character);
    }
}
