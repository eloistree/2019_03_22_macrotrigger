﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAsRepresentativeKeyboard : MonoBehaviour {

    public AbstractKeyboardState m_keyboard;
    void Awake()
    {
        KeystrokeUtility.Keyboards.SetKeyboardsAsRepresentative(m_keyboard);
    }

    void OnEnable()
    {
        KeystrokeUtility.Keyboards.SetKeyboardsAsRepresentative(m_keyboard);
    }
    
}
