﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using WindowsInput;
using WindowsInput.Native;

public class ListenToWindowKeyCodeThread : ListenToWindowKeyCode
{
    Thread m_checkKey;
    public int m_count;
    public int m_countFound;
    public long m_frame;
    // Start is called before the first frame update
    protected override void Awake()
    {
        m_checkKey = new Thread(CheckForStroke);
        m_checkKey.Priority = System.Threading.ThreadPriority.Lowest;
        m_checkKey.Start();

        m_keysState.onStateChange += DisplayChange;
    }

    private void DisplayChange(VirtualKeyCode element, bool isOn)
    {
        m_countFound++;
        //Debug.Log(">> " + element + " " + isOn);
        m_found.Enqueue(new KeyChangeFound(element, isOn));
    }

    public Queue<KeyChangeFound> m_found = new Queue<KeyChangeFound>();
    void CheckForStroke() {
        if (m_input == null)
            m_input = new InputSimulator();
        while(true){

            m_frame++;
            bool state;
            VirtualKeyCode kc;
            for (int i = 0; i < m_unityKeyCodeListened.Count; i++)
            {
                kc = m_unityKeyCodeListened[i];
                if (!m_useReal)
                    state = m_input.InputDeviceState.IsKeyDown(kc);
                else
                    state = m_input.InputDeviceState.IsHardwareKeyDown(kc);
               m_keysState.SetState(kc, state);
            }
        }
    }
    protected override void Update()
    {

        if (m_input == null)
            m_input = new InputSimulator();
        m_count = m_found.Count;
        while (m_found.Count>0) {
            KeyChangeFound found = m_found.Dequeue();
            if (found.m_state)
                m_onKeyCodeDown.Invoke(found.m_key);
            else
                m_onKeyCodeUp.Invoke(found.m_key);

            if (m_debug)
                Debug.Log("Listen to Virtual Key Code: " + found.m_key + "|" + found.m_state);

        }
    }

    [System.Serializable]
    public struct KeyChangeFound {
        public VirtualKeyCode m_key;
        public bool m_state;

        public KeyChangeFound(VirtualKeyCode kc, bool state) : this()
        {
            m_key = kc;
            m_state = state;
        }
    }

    private void OnDestroy()
    {
        if( m_checkKey!=null)
            m_checkKey.Abort();
    }
    void Reset()
    {
        m_unityKeyCodeListened = KeystrokeUtility.GetVirtualKeyCode();
    }
}
