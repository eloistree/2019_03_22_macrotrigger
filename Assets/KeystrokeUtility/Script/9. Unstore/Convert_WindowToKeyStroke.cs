﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

public class Convert_WindowToKeyStroke : MonoBehaviour
{
   
    [Header("Debug")]
    public  KeyboardTouch m_lastTouch;
    public bool m_lastConvertable;
    
    public  void OnReceivedNativeWindowKeyCode(VirtualKeyCode value, bool isDown)
    {

        KeyBindingTable.ConvertWindowVirtualKeyCodesToTouch(value,
            out m_lastTouch, 
            out m_lastConvertable);
        if (m_lastConvertable) {
            if(isDown)
                KeystrokeUtility.PressKeyDown(m_lastTouch);
            else
                KeystrokeUtility.PressKeyUp(m_lastTouch);
        }
    }
}
