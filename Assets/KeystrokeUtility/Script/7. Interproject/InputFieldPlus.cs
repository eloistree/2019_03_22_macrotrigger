﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(InputFieldPlus))]
public class InputFieldPlus : MonoBehaviour {

    private InputField m_field;

    public int Length { get { return m_field.text.Length; } }

    // Use this for initialization
    void Start () {
        m_field = GetComponent<InputField>();		
	}

    // Update is called once per frame
    public void ApprehendAtStart(string text, bool lineReturn=true)
    {
        m_field.text = text + m_field.text + (lineReturn ? "\n\r" : "");
    }
    public void ApprehendAtEnd(string text, bool lineReturn = true)
    {
        m_field.text = m_field.text + text + (lineReturn ? "\n\r" : "");
    }
    public void Clear() { m_field.text = ""; }
    public void Trim() { m_field.text = m_field.text.Trim(); }
    public void Lower() { m_field.text = m_field.text.ToLower(); }
    public void Upper() { m_field.text = m_field.text.ToUpper(); }
}
