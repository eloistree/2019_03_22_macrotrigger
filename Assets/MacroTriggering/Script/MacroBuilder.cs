﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System;

public class MacroBuilder : MonoBehaviour
{

    public TriggersManager m_triggerManager;
    public GameObject m_linearConditionPrefab;
    public GameObject m_linearConditionStatePrefab;
    public IdNameToMacro m_idToMacro;
    public IdToFonction m_idToFonction;
    public MorseToMacro m_morseToMacro;
    public GameObject m_morsePrefab;

    public Text m_tex;
    // Start is called before the first frame update
    void Start()
    {
        GameObject morseGroup = new GameObject("Morse");
        GameObject condGroup = new GameObject("Conditional");

        string dataPath = Application.dataPath;

        dataPath = Application.dataPath + "/../";

        string path = Path.GetDirectoryName(dataPath + "/MacroQuickBuilder/Morse/");
        m_tex.text = name + " -- " + path;
        Directory.CreateDirectory(path);
        string [] m_files = Directory.GetFiles(path);
        for (int i = 0; i < m_files.Length; i++)
        {
            string name = Path.GetFileName(m_files[i]);

            if (!(name.IndexOf(".ahk") > -1 || name.IndexOf(".exe") > -1 || name.IndexOf(".bat") > -1))
                continue;


            Debug.Log(">" + name);
            int splimterIndex = name.IndexOf("_");
            if (splimterIndex < 0)
                continue;




            string touch = name.Substring(0, name.IndexOf("_"));
            string morse = name.Substring(name.IndexOf("_")+1);
            morse = RemoveExtension(morse);
            morse = morse.Replace('1', '.');
            morse = morse.Replace('2', '-');
            Debug.Log(touch + " -> " + morse);
            KeyboardTouch t = KeyBindingTable.ConvertStringToTouch(touch);
            List<MorseEmittor_KeyStrokeListener> le = GameObject.FindObjectsOfType<MorseEmittor_KeyStrokeListener>().Where(k => k.m_keypressed == t).ToList();
            if (le.Count<= 0) {
                GameObject obj = GameObject.Instantiate(m_morsePrefab);
                obj.name = name;
                MorseEmittor_KeyStrokeListener morseEmittor = obj.GetComponentInChildren<MorseEmittor_KeyStrokeListener>();
                morseEmittor.m_keypressed = t;
            }
            m_idToMacro.m_nameToMacros.Add(new IdNameToMacro.InfoNameToMacro() { m_name = RemoveExtension(name), m_filePath = m_files[i] });
            m_morseToMacro.m_links.Add(new MorseToMacro.MorseLinkedToMacroByKeystroke() { m_keyCode = t, m_morse = new MorseValue(morse), m_macroName = RemoveExtension(name) });
            m_idToFonction.m_idToFonctions.Add(new IdToFonction.InfoNameToFonctionsToTrigger() { m_name = RemoveExtension(name), m_filePath = m_files[i].Replace(".ahk", ".mtfct") });
            m_tex.text = name + " -- " + m_files[i];



        }



        path = Path.GetDirectoryName(dataPath + "/MacroQuickBuilder/Sequence/");
        Directory.CreateDirectory(path);
        m_files = Directory.GetFiles(path);
        for (int i = 0; i < m_files.Length; i++)
        {
            string name = Path.GetFileName(m_files[i]);

            if (!(name.IndexOf(".ahk") > -1 || name.IndexOf(".exe") > -1 || name.IndexOf(".bat") > -1))
                continue;


            GameObject obj = GameObject.Instantiate(m_linearConditionPrefab);
            obj.name = name;
            LinearConditionStateMachine conditional = obj.GetComponent<LinearConditionStateMachine>();
            m_triggerManager.m_triggersPrefab.Add(conditional);
            conditional.m_idName = RemoveExtension(name);
            m_idToMacro.m_nameToMacros.Add(new IdNameToMacro.InfoNameToMacro() { m_name = RemoveExtension(name), m_filePath = m_files[i] });
            m_idToFonction.m_idToFonctions.Add(new IdToFonction.InfoNameToFonctionsToTrigger() { m_name = RemoveExtension(name), m_filePath = m_files[i].Replace(".ahk",".mtfct") });

            obj.transform.parent = condGroup.transform;

            Debug.Log(">" + name);
            string value = name;
            value = RemoveExtension(value);

          

            string[] sequence = value.Split('_');
            conditional.m_conditions.Clear();
            foreach (string  s in sequence)
            {
                string[] touch = s.Split('+');
                for (int j = 0; j < touch.Length; j++)
                {
                    touch[j] = KeyBindingTable.ConvertStringToTouch(touch[j]).ToString() ;
                }

                GameObject state = GameObject.Instantiate(m_linearConditionStatePrefab);
                StateCondition_Entries entry = state.GetComponent<StateCondition_Entries>();
                state.transform.parent = obj.transform;
                entry.m_observedEntries = touch;
                conditional.m_conditions.Add(entry);
            }
            

            //string fctFilePath = path + value + ".mtfct";
            //if (File.Exists(fctFilePath))
            //{
            //    string[] fileLine = File.ReadAllLines(fctFilePath);

            //    if (fileLine.Length > 0)
            //    {
            //        string fctName = fileLine[0];
            //        string[] parameters = new string[fileLine.Length - 1];
            //        for (int j = 1; j < fileLine.Length; j++)
            //        {
            //            parameters[j - 1] = fileLine[j];
            //        }
            //    }
            //}

        }



    }

    private string RemoveExtension(string morse)
    {
        morse = morse.Replace(".ahk", "");
        morse = morse.Replace(".exe", "");
        morse = morse.Replace(".bat", "");
        return morse;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
