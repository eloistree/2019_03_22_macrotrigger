﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IdNameToMacro : MonoBehaviour
{
    public Text m_debug;
    public void TryToLaunch(string name) {
        for (int i = 0; i < m_nameToMacros.Count; i++)
        {
            if (m_nameToMacros[i].m_name == name) {
                AhkFileLauncher.LaunchAHK(m_nameToMacros[i].m_filePath, "");
                m_debug.text = name + " >> " + m_nameToMacros[i].m_filePath;
            }

        }
    }

    public List<InfoNameToMacro> m_nameToMacros;


    [System.Serializable]
    public struct InfoNameToMacro
    {
        public string m_name;
        public string m_filePath;
    }

    public bool m_launchable = true;
    public void SetLaunchableState(bool onOff) {
        m_launchable = onOff;
    }

    public void SwitchLaunchableState() {
        m_launchable = !m_launchable;
    }
}
