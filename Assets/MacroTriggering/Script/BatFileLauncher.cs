﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class BatFileLauncher : MonoBehaviour
{
    public string m_filePath;

    void Start()
    {
        LaunchBat(m_filePath,"");
    }

    public static void LaunchBat(string absolutPath, string arg) {
     
        String path = @absolutPath;
        Process foo = new Process();
        foo.StartInfo.FileName = path;
        foo.StartInfo.Arguments = arg;
        foo.Start();
        

    }
}
