﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using UnityEngine;

public class AhkFileLauncher : MonoBehaviour
{
    public string m_ahkExePath;

    public static string m_choosedAhkExePath;

    public string test;
    public KeyboardTouch kin, kou;
    public static float tin, tout;
    public float timeTouchDown =1;
    public AbstractKeyboardState k;
    public bool m_debug;
    private void Update()
    {
        k = (AbstractKeyboardState) KeystrokeUtility.Keyboards.GetRepresentativeKeyboard();
        if (k.IsTouchDown(kin, timeTouchDown)) {
            tin = Time.time;
            if(m_debug)
            UnityEngine.Debug.Log(">In: " + Time.time);
            LaunchAHK(test, "");
        }
        if (k.IsTouchDown(kou, timeTouchDown))
        {
            tout = Time.time;
            if (m_debug)
            {
                UnityEngine.Debug.Log(">Out: " + Time.time);
                UnityEngine.Debug.Log(">Result: " + (tout - tin));
            }
        }
        if (k.IsTouchActive(KeyboardTouch.C))
        {
            UnityEngine.Debug.Log(">Hey mon ami");

        }

    }


    void Start()
    {
        KeystrokeUtility.InvokeDefaultListener();
        m_choosedAhkExePath = m_ahkExePath;
    }

    public static string GetPath(string pathGiven) {
        if ( File.Exists(pathGiven) && Path.IsPathRooted(pathGiven))
            return pathGiven;
        string path = Path.GetDirectoryName(Application.dataPath) ;
        Directory.CreateDirectory(path);
        path += "/"+pathGiven;
        UnityEngine.Debug.Log("What ?:" + path);
        if (File.Exists(path))
            return path;
        return null;

    }

    public static void LaunchAHK(string absolutPath, string arg)
    {
        if (!string.IsNullOrEmpty(m_choosedAhkExePath))
        {

        }
        else {

            string path = GetPath(absolutPath);
            UnityEngine.Debug.Log("AhK path:" + path);
            if (string.IsNullOrEmpty(path))
                return;
            Process foo = new Process();
            foo.StartInfo.FileName = path;
            foo.StartInfo.Arguments = arg;
            foo.Start();

        }


    }

                            
}
