﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class IdToFonction : MonoBehaviour
{

    public Text m_debug;
    public List<InfoNameToFonctionsToTrigger> m_idToFonctions;


    public List<IdToUnityEvent> m_listOfFonction = new List<IdToUnityEvent>();

    [System.Serializable]
    public class IdToUnityEvent
    {
        public string m_id;
        public UnityEvent m_unityEvent;
        public FunctionableFullEvent m_functionableEvent;
        public Functionable[] m_targets;
    }
    [System.Serializable]
    public struct InfoNameToFonctionsToTrigger
    {
        public string m_name;
        public string m_filePath;
    }



    public void TryToLaunch(string name)
    {
        for (int i = 0; i < m_idToFonctions.Count; i++)
        {
            if (m_idToFonctions[i].m_name == name)
            {
                PlayFonctionInFiles(m_idToFonctions[i].m_filePath);
                m_debug.text = name + " >> " + m_idToFonctions[i].m_filePath;
            }

        }
    }

    private void PlayFonctionInFiles(string m_filePath)
    {
        if (!File.Exists(m_filePath))
            return;
        string[] lines = File.ReadAllLines(m_filePath);
        foreach (string l in lines)
        {
            int fctStart = l.IndexOf('(');
            int fctEnd = l.LastIndexOf(')');
            if (fctStart >= 0 && fctEnd >= 0 && fctStart<fctEnd)
            {
                string fctName = l.Substring(0,fctStart) ;
                string[] parameters = l.Substring(fctStart+1, fctEnd - fctStart-1).Split(',');
                Debug.Log(">>> " + fctName+ " --> "+ string.Concat(parameters));
                TryToLaunchFct(fctName, parameters);
            }


        }
    }

    public void TryToLaunchFct(string name)
    {
        TryToLaunchFct(name, null);
    }

    public void TryToLaunchFct(string name, string  [] parameters )
    {




        for (int i = 0; i < m_listOfFonction.Count; i++)
        {
            if (m_listOfFonction[i].m_id == name)
            {

                m_debug.text = name + " >Fct> " + m_listOfFonction[i].m_id;
                m_listOfFonction[i].m_unityEvent.Invoke();
                m_listOfFonction[i].m_functionableEvent.Invoke(name, parameters);
                foreach (var item in m_listOfFonction[i].m_targets)
                {
                    item.CallFunction(name, parameters);
                }

            }

        }
    }

    public void AddFonction(string idName) {
        IdToUnityEvent idEvent = FindUnityEvent(idName);
        if (idEvent == null)
            idEvent = CreateEvent(idName);
      

    }

    private IdToUnityEvent CreateEvent(string idName)
    {
        IdToUnityEvent e = new IdToUnityEvent() { m_id = idName, m_unityEvent = new UnityEvent() };
        m_listOfFonction.Add(e);
        return e;
    }

    private IdToUnityEvent FindUnityEvent(string idName)
    {
        for (int i = 0; i < m_listOfFonction.Count; i++)
        {
            if (m_listOfFonction[i].m_id == name)
            {
                return m_listOfFonction[i];
            }

        }
        return null;
    }
    public bool m_launchable=true;
    public void SetLaunchableState(bool onOff)
    {
        m_launchable = onOff;
    }

    public void SwitchLaunchableState()
    {
        m_launchable = !m_launchable;
    }
}
