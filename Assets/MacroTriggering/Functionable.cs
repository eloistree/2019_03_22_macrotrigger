﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IFunctionable {

  void CallFunction(string name, params string [] parameters);
  string [] GetFonctionsTakeInCharge();
}

public abstract class Functionable : MonoBehaviour, IFunctionable
{
    public void CallFunction(string fctName, params string[] parameters)
    {
        foreach (string  inCharge in GetFonctionsTakeInCharge())
        {
            if (fctName == inCharge) {
                 SendMessage(fctName, parameters, SendMessageOptions.DontRequireReceiver);
            }
        }
    }   
    public abstract string[] GetFonctionsTakeInCharge();
}

[System.Serializable]
public class FunctionableEvent : UnityEvent< string[]>
{

}
[System.Serializable]
public class FunctionableFullEvent : UnityEvent<string, string[]>
{

}