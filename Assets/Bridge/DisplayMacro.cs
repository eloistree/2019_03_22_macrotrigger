﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayMacro : MonoBehaviour
{

    public Text m_text;
    public Text m_entry;
    public Text m_macro;
    public MorseToMacro m_morseToMacro;
    public TriggersManager m_triggerManager;

    public void SetEntry(EntryEvent entry)
    {
        m_entry.text = entry.m_idName + " " + entry.m_activeState;

    }
  
    public void SetMacro(string entry)
    {

        m_macro.text = entry;
    }

    void Start()
    {
//        for (int i = 0; i < m_morseToMacro.m_links.Length; i++)
//        {
//            m_text.text = string.Format("{0} - {1} - {2} ",
//            m_morseToMacro.m_links[i].m_keyCode,
//m_morseToMacro.m_links[i].m_macroName,
//m_morseToMacro.m_links[i].m_morse);

//        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
