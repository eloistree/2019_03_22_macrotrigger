﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteAfterMorse : MonoBehaviour
{

    public void ReceivedMorse(MorseValueWithOrigine morse)
    {
        for (int i = 0; i < morse.GetMorseValue().GetValue().Length; i++)
        {
            KeystrokeUtility.PressKeyDown(KeyboardTouch.Backspace);
            KeystrokeUtility.PressKeyUp(KeyboardTouch.Backspace);
        }
    }
}
