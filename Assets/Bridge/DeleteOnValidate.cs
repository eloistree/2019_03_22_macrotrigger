﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteOnValidate : MonoBehaviour
{
    public int m_count=1;
    public void RemoveCharacter() {
        for (int i = 0; i < m_count; i++)
        {
            KeystrokeUtility.PressKeyDown(KeyboardTouch.Backspace);
            KeystrokeUtility.PressKeyUp(KeyboardTouch.Backspace);
        }
    }
}
