﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NativeKeyboardEntryEmitter : AbstractEntryEmitter
{
    //public KeyboardTouch [] m_listenedKey= new KeyboardTouch[] { KeyboardTouch.Left, KeyboardTouch.Right, KeyboardTouch.Down, KeyboardTouch.Up };
    public void Awake()
    {
        KeystrokeUtility.InvokeDefaultListener();
        foreach (KeyboardTouch kcode in Enum.GetValues(typeof(KeyboardTouch)))
        {
            m_usedState.Add(kcode, false);

        }
    }
  
    private void Update()
    {
        foreach (KeyboardTouch kcode in Enum.GetValues(typeof(KeyboardTouch)))
        {
          
            if (m_usedState[kcode] == true && KeystrokeUtility.Keyboards.GetRepresentativeKeyboard().IsTouchUp(kcode))
            {
                m_usedState[kcode] = false;
                NotifyEntryEvent(EntryEvent.Create(kcode.ToString(), false));
               
            }

        }
        foreach (KeyboardTouch kcode in Enum.GetValues(typeof(KeyboardTouch)))
        {
            if (m_usedState[kcode] == false && KeystrokeUtility.Keyboards.GetRepresentativeKeyboard().IsTouchActive(kcode))
            {
                m_usedState[kcode] = true;
                NotifyEntryEvent(EntryEvent.Create(kcode.ToString(), true));
            }

        }
    }

    public Dictionary<KeyboardTouch, bool> m_usedState = new Dictionary<KeyboardTouch, bool>();
}
