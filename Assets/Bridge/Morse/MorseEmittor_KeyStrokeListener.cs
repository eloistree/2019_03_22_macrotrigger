﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MorseEmittor_KeyStrokeListener : MorseEmittorAbstract
{
    public KeyboardTouch m_keypressed = KeyboardTouch.Space;
    public bool m_state;
    public void Awake()
    {
        KeystrokeUtility.InvokeDefaultListener();
    }
    public override bool IsEmitting()
    {
        return m_state = KeystrokeUtility.Keyboards.GetRepresentativeKeyboard().IsTouchActive(m_keypressed);
    }

    public void OnValidate()
    {
        SetKeyboardListened(m_keypressed);
    }

    internal void SetKeyboardListened(KeyboardTouch keyCodeId)
    {
        m_keypressed = keyCodeId;
        m_emittorName = "Touch Stroke " + m_keypressed.ToString();

    }
}
