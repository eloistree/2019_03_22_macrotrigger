﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowsInput.Native;

public class Demo_KeyboardListener : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        KeystrokeUtility.onKeyDown += DoTheStuff;        
    }

    private void DoTheStuff(KeyboardTouchPressRequest touch)
    {

    }
    public void Update()
    {
        if ( KeystrokeUtility.Keyboards.GetRepresentativeKeyboard().IsTouchActive(KeyboardTouch.Space))
            Debug.Log("Hey :) : " +Time.time);
    }
}
