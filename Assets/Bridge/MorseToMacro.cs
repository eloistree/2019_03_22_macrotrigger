﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;

public class MorseToMacro : MonoBehaviour
{
    public OnMacroToCall m_onMacroToCall;
    public List<MorseLinkedToMacroByKeystroke> m_links;

    [System.Serializable]
    public struct MorseLinkedToMacroByKeystroke {
        public KeyboardTouch m_keyCode;
        public MorseValue m_morse;
        public string m_macroName;
    }

  

    public void OnMorseReceived(MorseValueWithOrigine morse) {

        for (int i = 0; i < m_links.Count; i++)
        {
            if (morse.GetMorseValue() == m_links[i].m_morse)
            {

                MorseEmittor_KeyStrokeListener keyListener =
                    morse.GetEmittorSource() as MorseEmittor_KeyStrokeListener;
                if (keyListener != null)
                {
                    if (keyListener.m_keypressed == m_links[i].m_keyCode)
                    {

                        m_onMacroToCall.Invoke(m_links[i].m_macroName);

                        for (int j = 0; j < morse.GetMorseValue().GetValue().Length; j++)
                        {
                            KeystrokeUtility.PressKeyDown(KeyboardTouch.Backspace);
                            KeystrokeUtility.PressKeyUp(KeyboardTouch.Backspace);
                        }






                    }
                }

            }
        }
    }
    
    [System.Serializable]
    public class OnMacroToCall : UnityEvent<string> { }
}
