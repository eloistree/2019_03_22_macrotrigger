﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioUtility : Functionable
{

    public AudioSource m_beep;
    public AudioSource m_singleSound;
    public AudioClip [] m_unityClips;
    public override string[] GetFonctionsTakeInCharge()
    {
        return new string[] { "PlayBeep", "PlaySound" };
    }

    public void PlayBeep(string  [] paramaters)
    {
        m_beep.Play();
    }


    public void PlaySound(string[] paramaters)
    {
        if (paramaters.Length == 0)
        {
            for (int i = 0; i < m_unityClips.Length; i++)
            {
                if (m_unityClips[i].name == paramaters[0])
                {
                    m_singleSound.clip = m_unityClips[i];
                    m_singleSound.Play();
                }
            }
        }

    }
}
